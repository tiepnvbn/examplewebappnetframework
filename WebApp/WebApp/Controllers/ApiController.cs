﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class ApiController : Controller
    {
        // GET: api/greetingservice
        [HttpGet]
        public ActionResult GreetingService(string name, string age)
        {
            var message = new
            {
                Id = "1" , Name = name, Age = age
            };
            return Json(message, JsonRequestBehavior.AllowGet);
        }
        
    }
}
